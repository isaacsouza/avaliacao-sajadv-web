app.controller("ResponsavelController", function ($scope, ResponsavelService) {

	$scope.itemSize = [10,25,50,100];
    $scope.size = $scope.itemSize[0];
    $scope.filter = new ResponsavelFilter();
    $scope.totalCount = 0;
    $scope.page = 1;

	$scope.findResponsaveis = function (filter) {
		var responsaveis = ResponsavelService.findResponsaveis(filter.nome, filter.cpf, filter.numeroProcessoUnificado,
		$scope.size, $scope.page - 1);
		responsaveis.then(function (response) {
			$scope.responsaveis = response.data.content;
            $scope.totalCount = response.data.totalElements;
		}, function() {
			alert('Erro ao pesquisar responsaveis');
		});
	}

    $scope.pageChanged = function(newPage) {
        $scope.page = newPage;
        $scope.findResponsaveis($scope.filter);
    };

    $scope.clearFilter = function(){
		$scope.filter = new ResponsavelFilter();
	};

	$scope.newResponsavel = function () {
		$scope.responsavel = new Responsavel();
		$scope.showModal = true;
		$scope.foto = "img/photo.png";
		$scope.action = "create";
	}

	$scope.editResponsavel = function (responsavel) {
        $scope.responsavel = responsavel;
        $scope.foto = $scope.responsavel.foto;
        $scope.showModal = true;
        $scope.action = "update";
	}

	$scope.saveResponsavel = function () {
		if($scope.action == "update"){
			var responsavelSaved = ResponsavelService.updateResponsavel($scope.responsavel.id, $scope.responsavel);
			responsavelSaved.then (function (response) {
                    $scope.findResponsaveis($scope.filter);
                    alert("Responsavel Salvo!");
                }, function () {
                    alert('Erro ao criar Responsavel.');
                }
            );
		}else{
			var responsavelSaved = ResponsavelService.createResponsavel($scope.responsavel);
			responsavelSaved.then (function (response) {
                    $scope.findResponsaveis($scope.filter);
                    alert("Responsavel Salvo!");
                }, function () {
                    alert('Erro ao atualizar o Responsavel.');
                }
			);
		}
		$scope.showModal = false;
	}

	$scope.deleteResponsavel = function (responsavel) {
		var confirmed = confirm('Você tem certeza que deseja deletar o Responsável: '+responsavel.nome+' ?');
		if(confirmed) {
			var responsavelDeleted = ResponsavelService.deleteResponsavel(responsavel.id);
			responsavelDeleted.then (function (response) {
                    $scope.findResponsaveis($scope.filter);
            alert("Responsavel Deletado!");
                }, function () {
                    alert('Erro ao deletar Responsavel.');
                }
            );
		}
	}

    $scope.cancel = function() {
      $scope.showModal = false;
    };

    $scope.changeFoto = function(foto) {
      if (foto.filesize > 50000){
        alert("A foto deve ter no máximo 50kb.")
        return;
      }
      $scope.foto = "data:"+ foto.filetype + ";utf8;base64," + foto.base64;
      $scope.responsavel.foto = $scope.foto;
    };

});