app.controller("ProcessoController", function ($scope, ProcessoService, SituacaoService, ResponsavelService) {

	$scope.itemSize = [10,25,50,100];
    $scope.size = $scope.itemSize[0];
    $scope.filter = new ProcessoFilter();
    $scope.totalCount = 0;
    $scope.page = 1;
    $scope.situacoes = [];
    $scope.responsaveisResumido = [];
    $scope.responsavelSelecionado = {};
    $scope.processosResumido = [];


    $scope.findAllSituacoes = function () {
		var situacoes = SituacaoService.findAllSituacoes();
		situacoes.then(function (response) {
			$scope.situacoes = response.data;
		}, function() {
			alert('Erro ao carregar situacoes.');
		});
	}
    $scope.findAllSituacoes();

    $scope.findAllResponsaveisResumido = function () {
		var responsaveis = ResponsavelService.findAllResponsaveisResumido();
		responsaveis.then(function (response) {
			$scope.responsaveisResumido = response.data;
		}, function() {
			alert('Erro ao carregar responsaveis (resumido).');
		});
	}


    $scope.findAllProcessosResumido = function () {
		var processos = ProcessoService.findAllProcessosResumido();
		processos.then(function (response) {
			$scope.processosResumido = response.data;
		}, function() {
			alert('Erro ao carregar processos (resumido).');
		});
	}

	$scope.loadDadosAuxiliares = function(){
	    if ($scope.responsaveisResumido.length == 0) {
	        $scope.findAllResponsaveisResumido();
	    }

	    if ($scope.processosResumido.length == 0) {
	        $scope.findAllProcessosResumido();
	    }
	}

	$scope.findProcessos = function (filter) {
		var processos = ProcessoService.findProcessos(
		    filter.numeroProcessoUnificado, dateToISO(filter.dataDistribuicaoInicial),
            dateToISO(filter.dataDistribuicaoFinal), filter.segredoJustica, filter.pastaFisicaCliente,
            filter.idSituacao, filter.nomeResponsavel,
		    $scope.size, $scope.page - 1);
		processos.then(function (response) {
			$scope.processos = response.data.content;
            $scope.totalCount = response.data.totalElements;
		}, function() {
			alert('Erro ao pesquisar processos');
		});
	}

    $scope.pageChanged = function(newPage) {
        $scope.page = newPage;
        $scope.findProcessos($scope.filter);
    };

    $scope.clearFilter = function(){
		$scope.filter = new ProcessoFilter();
	};

	$scope.newProcesso = function () {
	    $scope.loadDadosAuxiliares();
		$scope.processo = new Processo();
		$scope.showModal = true;
		$scope.foto = "img/photo.png";
		$scope.action = "create";
	}

	$scope.editProcesso = function (processo) {
	    $scope.loadDadosAuxiliares();
	    processo.idSituacao = processo.situacao.id;
        $scope.processo = processo;
        $scope.processo.dataDistribuicao = new Date(processo.dataDistribuicao+"T03:00:00Z");
        $scope.showModal = true;
        $scope.action = "update";
	}

	$scope.saveProcesso = function () {
	    $scope.processo.dataDistribuicao = dateToISO($scope.processo.dataDistribuicao);
		if($scope.action == "update"){
		    if ($scope.processo.id != ''){
                if ($scope.processo.id == $scope.processo.idProcessoPai){
                    alert("Não é possível informar o próprio processo como sendo o processo pai!");
                    return;
                }
            }

			var processoSaved = ProcessoService.updateProcesso($scope.processo.id, $scope.processo);
			processoSaved.then (function (response) {
                    $scope.findProcessos($scope.filter);
                    alert("Processo Salvo!");
                }, function () {
                    alert('Erro ao criar Processo.');
                }
            );
		}else{
			var processoSaved = ProcessoService.createProcesso($scope.processo);
			processoSaved.then (function (response) {
                    $scope.findProcessos($scope.filter);
                    alert("Processo Salvo!");
                }, function () {
                    alert('Erro ao atualizar o Processo.');
                }
			);
		}
		$scope.showModal = false;
	}

	$scope.deleteProcesso = function (processo) {
		var confirmed = confirm('Você tem certeza que deseja deletar o Processo: '+processo.numeroProcessoUnificado+' ?');
		if(confirmed) {
			var processoDeleted = ProcessoService.deleteProcesso(processo.id);
			processoDeleted.then (function (response) {
                    $scope.findProcessos($scope.filter);
            alert("Processo Deletado!");
                }, function () {
                    alert('Erro ao deletar Processo.');
                }
            );
		}
	}

    $scope.cancel = function() {
      $scope.showModal = false;
    };

    $scope.addResponsavel = function(){
        $scope.processo.responsaveis.push($scope.responsavelSelecionado);
    }

    $scope.removeResponsavel = function(responsavel){
        $scope.processo.responsaveis.splice($scope.processo.responsaveis.indexOf(responsavel),1);
    }

});