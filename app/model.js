function Responsavel(){
    this.id = "";
    this.nome = "";
    this.cpf = "";
    this.email = "";
    this.foto = "";
    this.fotoLocal = "";
}

function ResponsavelFilter(){
    this.nome = "";
    this.cpf = "";
    this.numeroProcessoUnificado = "";
};

function Processo(){
    this.id = "";
    this.numeroProcessoUnificado = "";
    this.descricao = "";
    this.segredoJustica = "";
    this.pastaFisicaCliente = "";
    this.segredoJustica = false;
    this.situacao = "";
    this.idSituacao = "";
    this.dataDistribuicao = "";
    this.idProcessoPai = "";
    this.responsaveis = [];
    this.processosFilhos = "";
}

function ProcessoFilter(){
    this.numeroProcessoUnificado = "";
    this.dataDistribuicaoInicial = "";
    this.dataDistribuicaoFinal = "";
    this.segredoJustica = "";
    this.pastaFisicaCliente = "";
    this.idSituacao = "";
    this.nomeResponsavel = "";
};
