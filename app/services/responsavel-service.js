app.service("ResponsavelService", function ($http) {
	var serviceUrl = baseUrl + "/responsaveis";

    this.findResponsaveis = function (nome, cpf, numeroProcessoUnificado, size, page) {
        var response = $http({
			method  : "GET",
			url		: serviceUrl,
			params : {nome : nome, cpf : cpf, npu : numeroProcessoUnificado, size: size, page: page}
		});
		return response;
    };

    this.findAllResponsaveisResumido = function () {

        var response = $http({
			method  : "GET",
			url		: serviceUrl + "/resumido"
		});
		return response;
    };

	this.getResponsavel = function(id) {
		var response = $http({
			method	: "GET",
			url		: serviceUrl + "/" + id
		});
		return response;
	};

    this.createResponsavel = function (responsavel) {
		var response = $http({
			method  : "POST",
			url		: serviceUrl,
			data    : responsavel
		});
		return response;
	};

	this.updateResponsavel = function (id, responsavel) {
		var response = $http({
			method  : "PUT",
			url     : serviceUrl + "/" + id,
			data    : responsavel
		});
		return response;
	};

	this.deleteResponsavel = function (id) {
		var response = $http({
			method  : "DELETE",
			url		: serviceUrl + "/" + id
		});
		return response;
	};
});