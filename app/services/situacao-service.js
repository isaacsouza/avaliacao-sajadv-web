app.service("SituacaoService", function ($http) {
	var serviceUrl = baseUrl + "/situacoes";

    this.findAllSituacoes = function () {

        var response = $http({
			method  : "GET",
			url		: serviceUrl
		});
		return response;
    };

});