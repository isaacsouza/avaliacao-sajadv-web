app.service("ProcessoService", function ($http) {
	var serviceUrl = baseUrl + "/processos";

    this.findProcessos = function (numeroProcessoUnificado, dataDistribuicaoInicial,
        dataDistribuicaoFinal, segredoJustica, pastaFisicaCliente, idSituacao, nomeResponsavel,
        size, page) {

        var response = $http({
			method  : "GET",
			url		: serviceUrl,
			params  : {
			    npu : numeroProcessoUnificado,
			    dataDistribuicaoInicial : dataDistribuicaoInicial,
			    dataDistribuicaoFinal : dataDistribuicaoFinal,
			    segredoJustica : segredoJustica,
			    pastaFisicaCliente : pastaFisicaCliente,
			    idSituacao : idSituacao,
			    nomeResponsavel : nomeResponsavel,
			    size: size,
			    page: page}
		});
		return response;
    };

    this.findAllProcessosResumido = function () {

        var response = $http({
			method  : "GET",
			url		: serviceUrl + "/resumido"
		});
		return response;
    };

	this.getProcesso = function(id) {
		var response = $http({
			method	: "GET",
			url		: serviceUrl + "/" + id
		});
		return response;
	};

    this.createProcesso = function (processo) {
		var response = $http({
			method  : "POST",
			url		: serviceUrl,
			data    : processo
		});
		return response;
	};

	this.updateProcesso = function (id, processo) {
		var response = $http({
			method  : "PUT",
			url     : serviceUrl + "/" + id,
			data    : processo
		});
		return response;
	};

	this.deleteProcesso = function (id) {
		var response = $http({
			method  : "DELETE",
			url		: serviceUrl + "/" + id
		});
		return response;
	};
});