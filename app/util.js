
function dateToISO(date){
    if (!date){
        return null;
    }
    return date.toISOString().slice(0, 19)
}